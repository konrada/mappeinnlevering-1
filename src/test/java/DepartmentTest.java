import org.junit.jupiter.api.Test;

public class DepartmentTest {


    @Test
    public void removePatientInDepartment() {
        Department department = new Department("Test");
        Patient patient = new Patient("Test", "Name", "123");
        department.addPatient(patient);
        try {
            department.remove(patient);
        } catch (RemoveException e) {
            System.err.println(e.getMessage());
        }
    }
    @Test
    public void RemovePatientNotInDepartment() {
        Department department = new Department("Test");
        Patient patient = new Patient("Test", "Name", "123");
        try {
            department.remove(patient);
        } catch (RemoveException e) {
            System.err.println(e.getMessage());
        }
    }
    @Test
    public void RemoveEmployeeInDepartment() {
        Department department = new Department("Test");
        Employee employee = new Employee("Test", "Name", "123");
        department.addEmployee(employee);
        try {
            department.remove(employee);
        } catch (RemoveException e) {
            System.err.println(e.getMessage());
        }
    }
    @Test
    public void RemoveEmployeeNotInDepartment() {
        Department department = new Department("Test");
        Employee employee = new Employee("Test", "Name", "123");
        try {
            department.remove(employee);
        } catch (RemoveException e) {
            System.err.println(e.getMessage());
        }
    }

}
