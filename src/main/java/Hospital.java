import java.util.ArrayList;

public class Hospital {
    private String hospitalName;
    private ArrayList<Department> departments;

    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        departments = new ArrayList<>();
    }

    public String getHospitalName() {
        return hospitalName;
    }
    public ArrayList getDepartments() {
        return departments;
    }
    public void addDepartment(Department department) {
        departments.add(department);
    }
    public String toString() {
        String res = hospitalName + " \n";
        res += "Departments: \n";
        for (int i = 0; i < departments.size(); i++) {
            res += departments.get(i).toString();
            res += "\n";
        }
        return res;
    }
}
