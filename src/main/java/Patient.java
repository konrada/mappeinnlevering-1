public class Patient extends Person implements Diagnosable{
    private String diagnosis = "";
    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
    public String toString() {
        return super.toString() + " " + diagnosis;
    }
}
