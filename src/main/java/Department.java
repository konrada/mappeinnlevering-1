import java.util.ArrayList;
import java.util.Objects;

public class Department {
    private String departmentName;
    private ArrayList patients;
    private ArrayList employees;

    public Department(String departmentName) {
        this.departmentName = departmentName;
        patients = new ArrayList();
        employees = new ArrayList();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    public ArrayList getPatients() {
        return patients;
    }
    public ArrayList getEmployees() {
        return employees;
    }
    public void addEmployee(Employee employee) {
        employees.add(employee);
    }
    public void addPatient(Patient patient) {
        patients.add(patient);
    }


    /**
     * Method that removes a person in the department
     * The method checks if the person is either a patient or employee in this department
     * if not, then the method throws a RemoveException
     * If the person exists within the department, the method iterates through either
     * the patients or employees and removes the person
     * @param person
     * @throws RemoveException
     */
    public void remove (Person person) throws RemoveException{
        if (!(patients.contains(person)) && !(employees.contains(person))) {
            throw new RemoveException("Person: " + person.toString() + "is not patient or employee in this department");
        }

        if (person instanceof Patient) {
            for (int i = 0; i < patients.size(); i++) {
                if (person.equals(i)) {
                    patients.remove(i);
                }
            }
        }
        if (person instanceof Employee) {
            for (int i = 0; i < employees.size(); i++) {
                if (person.equals(employees.get(i))) {
                    employees.remove(i);
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }
    public String toString() {
        String res = departmentName + " \n";
        res += "Employees: \n";
        for (int i = 0; i < employees.size(); i++) {
            res += employees.get(i).toString();
            res += "\n";
        }
        res += "Patients: \n";
        for (int i = 0; i < patients.size(); i++) {
            res += patients.get(i).toString();
            res += "\n";
        }
        return res;
    }
}
