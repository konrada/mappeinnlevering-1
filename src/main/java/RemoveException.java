public class RemoveException extends Exception{
    public RemoveException(String errorMessage) {
        super("Failed to remove person: " + errorMessage);
    }
}
