public class HospitalClient {
    public static void main(String[] args) {
        Hospital hospital = new Hospital("Sykehus");
        HospitalTestData.fillRegisterWithTestData(hospital);
        System.out.println(hospital.toString());
        Employee employee = (Employee) ((Department)hospital.getDepartments().get(0)).getEmployees().get(0);
        Patient patient = new Patient("Test", "Navn", "123");
        try {
            ((Department)hospital.getDepartments().get(0)).remove(employee);
        } catch (RemoveException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println(hospital.toString());
        }
        try {
            ((Department)hospital.getDepartments().get(0)).remove(patient);
        } catch (RemoveException e) {
            System.out.println(e.getMessage());
        }
    }
}
